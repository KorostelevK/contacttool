package com.example.korik.contacttool.interfaces;

/**
 * Будет реализован всеми презентерами, работающими по MVP
 */
public interface MvpPresenter<V extends MvpView> {

    void attachView(V mvpView);

    void viewIsReady();

    void detachView();

    void destroy();
}
