package com.example.korik.contacttool.views;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.korik.contacttool.MainPresenter;
import com.example.korik.contacttool.R;
import com.example.korik.contacttool.interfaces.ContactListContract;

public class MainActivity extends AppCompatActivity implements ContactListContract.MainView {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;

    private static int permissionRequestCount;
    private static boolean isPermissionGranted;

    private ContactListContract.MainViewPresenterIF mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainPresenter = new MainPresenter(this);

        checkPermission();

        if (isPermissionGranted) {
            mainPresenter.isReady();
        }
    }

    @Override
    public void initStartView() {
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            isPermissionGranted = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // Если запрос прерван, то архив будет пустой
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermissionGranted = true;
                } else {
                    if (++permissionRequestCount == 3) {
                        finish();
                    }
                    checkPermission();
                }
            }
        }
    }
}
