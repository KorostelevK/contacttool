package com.example.korik.contacttool.DB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.korik.contacttool.DB.dao.ContactDao;
import com.example.korik.contacttool.DB.dao.EmailDao;
import com.example.korik.contacttool.DB.dao.PhoneDao;
import com.example.korik.contacttool.DB.tables.Contact;
import com.example.korik.contacttool.DB.tables.Email;
import com.example.korik.contacttool.DB.tables.Phone;

@Database(entities = {Contact.class, Phone.class, Email.class}, version = 3)
public abstract class ContactsDB extends RoomDatabase {

    public abstract ContactDao contactDao();

    public abstract PhoneDao phoneDao();

    public abstract EmailDao emailDao();

}
