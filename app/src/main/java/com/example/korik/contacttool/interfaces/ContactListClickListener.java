package com.example.korik.contacttool.interfaces;

public interface ContactListClickListener {
    void onClick(String lookupKey);
}

