package com.example.korik.contacttool.interfaces;

import com.example.korik.contacttool.DB.tables.Contact;
import com.example.korik.contacttool.DB.tables.Email;
import com.example.korik.contacttool.DB.tables.Phone;

import java.util.List;

public interface ContactListContract {

    interface MainView extends MvpView {
        void initStartView();
    }

    interface MainViewPresenterIF {
        void isReady();
    }


    interface ContactView extends MvpView {

        // UI
        void showContactList(List<Contact> contactList);

//        void showContactDetailsFragment(String contactName, List<Phone> phones, List<Email> emails);

//        void showContactDetailsActivity(String contactName, List<Phone> phones, List<Email> emails);

        //        void subscribeUI();
//        Activity getViewActivity();

//        ContactDetailFragment getDetailsFragment();

        // other
//        boolean isTablet();

        // TODO: 09.06.2018
//        void showLoadingIndicator();
    }

    interface ContactListPresenterIF {

        void isReady();

        //        void load();
        void onContactClicked(String id);
    }

    interface Model {

        List<Contact> getContactsFromDB();

        List<Email> getEmailsFromDB(String lookupKey);

        List<Phone> getPhonesFromDB(String lookupKey);

        void putContactsToDB();

    }
}
