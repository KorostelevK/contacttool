package com.example.korik.contacttool;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.example.korik.contacttool.DB.ContactsDB;
import com.example.korik.contacttool.DB.dao.ContactDao;
import com.example.korik.contacttool.DB.dao.EmailDao;
import com.example.korik.contacttool.DB.dao.PhoneDao;
import com.example.korik.contacttool.DB.tables.Contact;
import com.example.korik.contacttool.DB.tables.Email;
import com.example.korik.contacttool.DB.tables.Phone;
import com.example.korik.contacttool.interfaces.ContactListContract;

import java.util.LinkedList;
import java.util.List;

public class Repository extends Application implements ContactListContract.Model {

    private static final String DB_NAME = "contactsDB";

    private static volatile Repository instance;

    private ContactsDB database;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(getApplicationContext(), ContactsDB.class, DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    public ContactsDB getDatabase() {
        return database;
    }

    public static Repository getInstance() {
        if (instance == null) {
            synchronized (Repository.class) {
                if (instance == null) {
                    instance = new Repository();
                }
            }
        }
        return instance;
    }

    @Override
    public List<Contact> getContactsFromDB() {
        ContactsDB db = getInstance().getDatabase();
        ContactDao contactDao = db.contactDao();
        return contactDao.getAll();
    }

    @Override
    public List<Email> getEmailsFromDB(String lookupKey) {
        ContactsDB db = getInstance().getDatabase();
        return db.emailDao().getByID(lookupKey);
    }

    @Override
    public List<Phone> getPhonesFromDB(String lookupKey) {
        ContactsDB db = getInstance().getDatabase();
        return db.phoneDao().getByID(lookupKey);
    }

    @Override
    public void putContactsToDB() {
        Context context = getApplicationContext();
        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        List<Contact> contactList = new LinkedList<>();
        List<Phone> phoneList = new LinkedList<>();
        List<Email> emailList = new LinkedList<>();
//        ContactsDB db = getInstance().getDatabase();
        ContactsDB db = getDatabase();

        try (Cursor cur = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC")) {
            if (cur != null) {
                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
//                    Log.d(LOG_TAG, "lookup_key");
//                    Log.d(LOG_TAG, id);

                    try (Cursor detailCur = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                            null, ContactsContract.Data.LOOKUP_KEY + " = ?",
                            new String[]{id}, null)) {

                        if (detailCur != null) {
                            while (detailCur.moveToNext()) {
                                String mime = detailCur.getString(detailCur.getColumnIndex(ContactsContract.Data.MIMETYPE));
                                switch (mime) {
                                    case ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE:
                                        String name = detailCur.getString(detailCur.getColumnIndex(
                                                ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                                        String patronymic = detailCur.getString(detailCur.getColumnIndex(
                                                ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME));
                                        String surname = detailCur.getString(detailCur.getColumnIndex(
                                                ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));

                                        Contact tempContact = new Contact(id, name, patronymic, surname);
                                        contactList.add(tempContact);
//                                        Log.d(LOG_TAG, "display name");
//                                        Log.d(LOG_TAG, tempContact.toString());

                                        break;

                                    case ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE:
                                        String phone = detailCur.getString(detailCur.getColumnIndex(ContactsContract.Data.DATA1));
                                        String phoneType = detailCur.getString(detailCur.getColumnIndex(ContactsContract.Data.DATA2));

                                        Phone tempPhone = new Phone(id, phone, convertType(context, phoneType));
                                        phoneList.add(tempPhone);
//                                        Log.d(LOG_TAG, "phone");
//                                        Log.d(LOG_TAG, tempPhone.toString());
                                        break;

                                    case ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE:
                                        String email = detailCur.getString(detailCur.getColumnIndex(ContactsContract.Data.DATA1));
                                        String emailType = detailCur.getString(detailCur.getColumnIndex(ContactsContract.Data.DATA2));

                                        Email tempEmail = new Email(id, email, convertType(context, emailType));
                                        emailList.add(tempEmail);
//                                        Log.d(LOG_TAG, "email");
//                                        Log.d(LOG_TAG, tempEmail.toString());
                                        break;

                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        ContactDao contactDao = db.contactDao();
        contactDao.insert(contactList);
        PhoneDao phoneDao = db.phoneDao();
        phoneDao.insert(phoneList);
        EmailDao emailDao = db.emailDao();
        emailDao.insert(emailList);
    }

    public static Contact getContactsFromDB(String lookupKey) {
        ContactsDB db = getInstance().getDatabase();
        ContactDao contactDao = db.contactDao();
        return contactDao.getByID(lookupKey);
    }

    public List<Email> getAllEmailsFromDB() {
        ContactsDB db = getDatabase();
        return db.emailDao().getAll();
    }


    public List<Phone> getAllPhonesFromDB() {
        ContactsDB db = getDatabase();
        return db.phoneDao().getAll();
    }

    private static String convertType(Context context, String type) {
        switch (type) {
            case "1":
                return context.getString(R.string.home_string);
            case "2":
                return context.getString(R.string.work_string);
            case "4":
                return context.getString(R.string.mobile_string);
            default:
                return context.getString(R.string.other_string);
        }
    }
}