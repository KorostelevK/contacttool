package com.example.korik.contacttool.DB.tables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Contact {

    @PrimaryKey
    @NonNull
    private String lookupKey;
    private String name;
    private String patronymic;
    private String surname;

    public Contact(@NonNull String lookupKey, String name, String patronymic, String surname) {
        this.lookupKey = lookupKey;
        this.name = name;
        this.patronymic = patronymic;
        this.surname = surname;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic == null ? "" : patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSurname() {
        return surname == null ? "" : surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append(name == null ? "" : name).append(" ")
                .append(patronymic == null ? "" : patronymic).append(" ")
                .append(surname == null ? "" : surname).toString();
    }
}
