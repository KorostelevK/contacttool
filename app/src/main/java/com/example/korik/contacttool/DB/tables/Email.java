package com.example.korik.contacttool.DB.tables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(foreignKeys = {
        @ForeignKey(entity = Contact.class
                , parentColumns = "lookupKey"
                , childColumns = "lookupKey"
                , onDelete = ForeignKey.CASCADE
                , onUpdate = ForeignKey.CASCADE)}
        , indices = {@Index(value = "lookupKey")})

public class Email implements Parcelable {

    public static final Parcelable.Creator<Email> CREATOR = new Parcelable.Creator<Email>() {
        // распаковываем объект из Parcel
        public Email createFromParcel(Parcel in) {
            return new Email(in);
        }

        public Email[] newArray(int size) {
            return new Email[size];
        }
    };

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String lookupKey;
    private String email;
    private String type;

    public Email(String lookupKey, String email, String type) {
        this.email = email;
        this.type = type;
        this.lookupKey = lookupKey;
    }

    private Email(Parcel parcel) {
        lookupKey = parcel.readString();
        email = parcel.readString();
        type = parcel.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new StringBuilder(type).append(" email: ").append(email).toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getLookupKey());
        dest.writeString(getEmail());
        dest.writeString(getType());
    }
}
