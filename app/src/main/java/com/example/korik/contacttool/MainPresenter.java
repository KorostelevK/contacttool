package com.example.korik.contacttool;

import android.support.v7.app.AppCompatActivity;

import com.example.korik.contacttool.interfaces.ContactListContract;
import com.example.korik.contacttool.views.MainActivity;

public class MainPresenter implements ContactListContract.MainViewPresenterIF {

    private AppCompatActivity activity;

    public MainPresenter(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void isReady() {

        Repository.getInstance().putContactsToDB();

        if (activity instanceof MainActivity) {
//            activity.setContentView(R.layout.activity_main);
//            Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
//            activity.setSupportActionBar(toolbar);
            ((MainActivity) activity).initStartView();
        }
    }
}
