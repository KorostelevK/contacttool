package com.example.korik.contacttool;

import com.example.korik.contacttool.interfaces.MvpPresenter;
import com.example.korik.contacttool.interfaces.MvpView;

/**
 * Базовый класс, который будет наследоваться всеми презентерами.
 * В нем реализованы общие методы по работе с View
 * Также можем поместить сюда механизмы для сбора RXJava подписок,
 * которые завершаются в destroy
 *
 */
public abstract class PresenterBase<T extends MvpView> implements MvpPresenter<T> {

    private T view;

    @Override
    public void attachView(T mvpView) {
        view = mvpView;
    }

    @Override
    public void detachView() {
        view = null;
    }

    public T getView() {
        return view;
    }

    protected boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void destroy() {

    }
}