package com.example.korik.contacttool;

import android.support.v4.app.Fragment;

import com.example.korik.contacttool.interfaces.ContactListContract;
import com.example.korik.contacttool.views.ContactsListFragment;

public class ContactListPresenter implements ContactListContract.ContactListPresenterIF {

    private Fragment fragment;

    public ContactListPresenter(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void isReady() {
        if (fragment instanceof ContactsListFragment) {

            ((ContactsListFragment) fragment).showContactList(Repository.getInstance().getContactsFromDB());
        }
    }

    @Override
    public void onContactClicked(String id) {

    }
}
