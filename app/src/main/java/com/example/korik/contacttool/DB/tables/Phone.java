package com.example.korik.contacttool.DB.tables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity
        (foreignKeys = {
                @ForeignKey(entity = Contact.class
                        , parentColumns = "lookupKey"
                        , childColumns = "lookupKey"
                        , onDelete = ForeignKey.CASCADE
                        , onUpdate = ForeignKey.CASCADE)}
                , indices = {@Index(value = "lookupKey")})
public class Phone implements Parcelable {

    public static final Parcelable.Creator<Phone> CREATOR = new Parcelable.Creator<Phone>() {
        // распаковываем объект из Parcel
        public Phone createFromParcel(Parcel in) {
            return new Phone(in);
        }

        public Phone[] newArray(int size) {
            return new Phone[size];
        }
    };

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String lookupKey;
    private String number;
    private String type;

    public Phone(String lookupKey, String number, String type) {
        this.number = number;
        this.type = type;
        this.lookupKey = lookupKey;
    }

    private Phone(Parcel parcel) {
        lookupKey = parcel.readString();
        number = parcel.readString();
        type = parcel.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new StringBuilder(type).append(" phone: ").append(number).toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getLookupKey());
        dest.writeString(getNumber());
        dest.writeString(getType());
    }
}
