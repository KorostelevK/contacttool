package com.example.korik.contacttool.DB.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.korik.contacttool.DB.tables.Contact;

import java.util.List;

@Dao
public interface ContactDao {

    @Query("SELECT * FROM contact")
    List<Contact> getAll();

    @Query("SELECT * FROM contact WHERE name LIKE :name " +
            "AND patronymic LIKE :patronymic AND surname LIKE :surname")
    List<Contact> getByFullName(String name, String patronymic, String surname);

    @Query("SELECT * FROM contact WHERE lookupKey LIKE :lookupKey ")
    Contact getByID(String lookupKey);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Contact contact);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Contact> contactList);

    @Insert
    void insertAll(Contact... contacts);

    @Delete
    void delete(Contact contact);

    @Update
    void update(Contact contact);
}
