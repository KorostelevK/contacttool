package com.example.korik.contacttool.DB.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.korik.contacttool.DB.tables.Email;

import java.util.List;

@Dao
public interface EmailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Email email);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Email> email);

    @Delete
    void delete(Email email);

    @Update
    void update(Email email);

    @Query("SELECT * FROM Email")
    List<Email> getAll();

    @Query("SELECT * FROM Email WHERE lookupKey = :lookupKey")
    List<Email> getByID(String lookupKey);
}
