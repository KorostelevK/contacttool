package com.example.korik.contacttool.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.korik.contacttool.ContactAdapter;
import com.example.korik.contacttool.ContactListPresenter;
import com.example.korik.contacttool.DB.tables.Contact;
import com.example.korik.contacttool.R;
import com.example.korik.contacttool.interfaces.ContactListContract;

import java.util.List;
import java.util.Objects;


public class ContactsListFragment extends Fragment implements ContactListContract.ContactView {

    private static ContactListContract.ContactListPresenterIF contactListPresenter;
    private RecyclerView contactsRecyclerView;

    public ContactsListFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.contact_list, container, false);
        contactsRecyclerView = view.findViewById(R.id.contact_list);
        assert contactsRecyclerView != null;
        LinearLayoutManager layoutManager = new LinearLayoutManager(Objects.requireNonNull(getActivity()).getBaseContext());
        contactsRecyclerView.setLayoutManager(layoutManager);
        contactListPresenter.isReady();
//        List<Contact> contactList = Repository.getInstance().getContactsFromDB();

//        contactsRecyclerView.setAdapter(new ContactAdapter(contactListPresenter.isReady(), getActivity()));

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactListPresenter = new ContactListPresenter(this);
        setHasOptionsMenu(true);

    }

//    private void checkPermissionAndLoadContacts() {
//        if (ContextCompat.checkSelfPermission(getContext(),
//                Manifest.permission.READ_CONTACTS)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(getActivity(),
//                    new String[]{Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
//        } else {
//            Repository.getInstance().putContactsToDB();
//            init();
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
//                // Если запрос прерван, то архив будет пустой
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    Repository.getInstance().putContactsToDB();
//                    init();
//
//                } else {
////                    if (permissionRequestCount == 3) {
////                        getActivity().finish();
////                    }
//                    checkPermissionAndLoadContacts();
//                }
//            }
//        }
//    }

    @Override
    public void showContactList(List<Contact> contactList) {
        contactsRecyclerView.setAdapter(new ContactAdapter(contactList, getActivity()));
    }


   /* @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        contactListViewModel = ViewModelProviders.of(this).get(ContactListViewModel.class);
        contactListViewModel.getContacts().observe(this, new Observer<List<ContactEntity>>() {
            @Override
            public void onChanged(@Nullable List<ContactEntity> contactEntities) {
                if (contactEntities != null) {
                    contactsListAdapter.setContactList(contactEntities);
                }
            }
        });

        setupPresenter();
        contactsListPresenter.requestReadContacts();

        if (checkSelfPermission()){
            contactsListPresenter.load();

            setDualPaneValue();
            if (savedInstanceState != null) {
                curCheckPosition = savedInstanceState.getInt("curChoice", 0);
            }
            if (isDualPane()) {
                startShowingDetails(curCheckPosition);
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("curChoice", curCheckPosition);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        contactsRecyclerView = null;
    }

    private void setupPresenter() {
        DataRepository repository = ((ContactsListApp) getActivity().getApplication()).getRepository();
        contactsListPresenter = new ContactsListPresenter(repository, new ImportService(getActivity()));
        contactsListPresenter.onAttach(this);
    }

    @Override
    public void onListFragmentInteraction(int position) {
        startShowingDetails(position);
    }

    void startShowingDetails(int index) {
        curCheckPosition = index;
        contactsListPresenter.showDetails(index);
    }


    @Override
    public Activity getViewActivity() {
        return getActivity();
    }

    @Override
    public void setContacts(List<Contact> contacts) {
    }

    @Override
    public void showContactDetailsFragment(String lookUpKey) {
        detailsFragment = DetailsFragment.newInstance(lookUpKey);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, detailsFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    @Override
    public void openContactDetailsActivity(String lookUpKey) {
        Intent mIntent = new Intent(getActivity(), DetailsActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putString(DETAILS_KEY, lookUpKey);
        mIntent.putExtras(mBundle);
        startActivity(mIntent);
    }

    @Override
    public DetailsFragment getDetailsFragment() {
        return detailsFragment;
    }

    @Override
    public boolean isDualPane() {
        return isDualPane;
    }

    private void setDualPaneValue(){
        View detailsFrame = getActivity().findViewById(R.id.fragment_container);
        isDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.main_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView =
                (SearchView) searchItem.getActionView();
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(onQueryTextListener);
    }

    private SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            getContactsByDisplayName(query);
//            CommonUtils.showToast(getActivity(), query);
            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            getContactsByDisplayName(newText);
            return false;
        }

    };

    private void getContactsByDisplayName(String search){
        search = "%"+search+"%";

        contactListViewModel.getByDisplayName(search).observe(this, new Observer<List<ContactEntity>>() {
            @Override
            public void onChanged(@Nullable List<ContactEntity> contactEntities) {
                if (contactEntities != null) {
                    contactsListAdapter.setContactList(contactEntities);
                }
            }
        });
    }*/
}

