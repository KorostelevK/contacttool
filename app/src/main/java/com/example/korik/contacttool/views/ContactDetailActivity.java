package com.example.korik.contacttool.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.example.korik.contacttool.R;

/**
 * Активити только  для смарфонов
 */
public class ContactDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Кнопка назад
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            // ооздаем фрагмент и добавляем его в активити с помощью транзакции
            Bundle arguments = new Bundle();

            arguments.putParcelableArrayList(ContactDetailFragment.PHONES_ID
                    , getIntent().getParcelableArrayListExtra(ContactDetailFragment.PHONES_ID));

            arguments.putParcelableArrayList(ContactDetailFragment.EMAILS_ID
                    , getIntent().getParcelableArrayListExtra(ContactDetailFragment.EMAILS_ID));

            arguments.putString(ContactDetailFragment.CONTACT_NAME,
                    getIntent().getStringExtra(ContactDetailFragment.CONTACT_NAME));
            ContactDetailFragment fragment = new ContactDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contact_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, MainActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
