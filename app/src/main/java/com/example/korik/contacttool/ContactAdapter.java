package com.example.korik.contacttool;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.korik.contacttool.DB.tables.Contact;
import com.example.korik.contacttool.DB.tables.Email;
import com.example.korik.contacttool.DB.tables.Phone;
import com.example.korik.contacttool.views.ContactDetailActivity;
import com.example.korik.contacttool.views.ContactDetailFragment;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    private final FragmentActivity mParentActivity;
    private final List<Contact> db;
    private final View.OnClickListener mOnClickListener;
/*
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String item_id = (String) view.getTag();

            Context context = view.getContext();

            Repository repository = Repository.getInstance();

//            List<Phone> phones = new ArrayList<>(RepositoryUtils.getPhonesFromDB(context, item_id));
//            List<Email> emails = new ArrayList<>(RepositoryUtils.getEmailsFromDB(context, item_id));
            List<Phone> phones = new ArrayList<>(repository.getPhonesFromDB (item_id));
            List<Email> emails = new ArrayList<>(repository.getEmailsFromDB (item_id));
            Contact contact =repository.getContactsFromDB(item_id);

            if (view.getResources().getBoolean(R.bool.isTablet)) {
                Bundle arguments = new Bundle();

                arguments.putParcelableArrayList(ContactDetailFragment.PHONES_ID
                        , (ArrayList<? extends Parcelable>) phones);
                arguments.putParcelableArrayList(ContactDetailFragment.EMAILS_ID
                        , (ArrayList<? extends Parcelable>) emails);

                arguments.putString(ContactDetailFragment.CONTACT_NAME, contact.toString());
                ContactDetailFragment fragment = new ContactDetailFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .commit();
            } else {
                Intent intent = new Intent(context, ContactDetailActivity.class);

                intent.putParcelableArrayListExtra(ContactDetailFragment.EMAILS_ID
                        , (ArrayList<? extends Parcelable>) emails);
                intent.putParcelableArrayListExtra(ContactDetailFragment.PHONES_ID
                        , (ArrayList<? extends Parcelable>) phones);

                intent.putExtra(ContactDetailFragment.CONTACT_NAME, contact.toString());
                context.startActivity(intent);
            }
        }
    };
*/
    public ContactAdapter(List<Contact> db, FragmentActivity parent, View.OnClickListener listener) {
        this.db = db;
        mParentActivity = parent;
        mOnClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Contact contact = db.get(position);

        String name = contact.getName();
        String patronymic = contact.getPatronymic();
        String surname = contact.getSurname();

        holder.name.setText(new StringBuilder().append(name).append(" ").append(patronymic).append(" ").append(surname).toString());
        holder.itemView.setTag(contact.getLookupKey());
        holder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return db.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView name;

        ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.item_text);
        }
    }
}