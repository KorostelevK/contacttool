package com.example.korik.contacttool.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.korik.contacttool.DB.tables.Email;
import com.example.korik.contacttool.DB.tables.Phone;
import com.example.korik.contacttool.R;

import java.util.List;

/**
 * Класс для показа фрагмента детальной информации
 */
public class ContactDetailFragment extends Fragment {

    public static final String CONTACT_NAME = "item_id";
    public static final String EMAILS_ID = "emails_id";
    public static final String PHONES_ID = "phones_id";

    private String id;
    private List<Phone> phones;
    private List<Email> emails;

    public ContactDetailFragment() {
    }

    private static String convertList(List<?> list) {
        StringBuilder string = new StringBuilder();

        if (list != null) {
            for (Object e : list) {
                string.append("\n").append(e.toString());
            }
        }
        return string.toString();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            if (getArguments().containsKey(EMAILS_ID)) {
                emails = getArguments().getParcelableArrayList(EMAILS_ID);
            }
            if (getArguments().containsKey(PHONES_ID)) {
                phones = getArguments().getParcelableArrayList(PHONES_ID);
            }
            if (getArguments().containsKey(CONTACT_NAME)) {
                id = getArguments().getString(CONTACT_NAME);
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.contact_detail, container, false);

        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout)
                this.getActivity().findViewById(R.id.toolbar_layout);
        if (appBarLayout != null) {
            appBarLayout.setTitle(id);
        }
        ((TextView) rootView.findViewById(R.id.contact_detail_phones)).setText(convertList(phones));
        ((TextView) rootView.findViewById(R.id.contact_detail_emails)).setText(convertList(emails));
        return rootView;
    }
}
