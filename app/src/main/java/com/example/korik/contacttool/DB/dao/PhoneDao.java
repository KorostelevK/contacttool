package com.example.korik.contacttool.DB.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.korik.contacttool.DB.tables.Phone;

import java.util.List;

@Dao
public interface PhoneDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Phone phone);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Phone> phone);

    @Delete
    void delete(Phone phone);

    @Update
    void update(Phone phone);

    @Query("SELECT * FROM Phone")
    List<Phone> getAll();


    @Query("SELECT * FROM Phone WHERE lookupKey = :lookupKey")
    List<Phone> getByID(String lookupKey);
}
